#!/usr/bin/env python3

import unittest
import doctest

from iso import iso8601


def load_tests(loader, suite, ignore):
    suite.addTests(doctest.DocTestSuite(iso8601))
    suite.addTest(doctest.DocFileSuite('test.txt'))
    return suite


def test():
    unittest.main(verbosity=2)


if __name__ == '__main__':
    test()
