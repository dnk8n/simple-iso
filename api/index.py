#!/usr/bin/env python3

from datetime import datetime, timezone

from flask import Flask, jsonify, redirect
from iso import iso8601

app = Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/')
def index():
    return redirect(f'/8601/{datetime.now(timezone.utc).astimezone().isoformat()}')


@app.route('/8601/<timestamp>')
def get_iso8601_api(timestamp: str):
    try:
        return jsonify(iso8601.get_datetime_dict(iso8601.get_datetime(timestamp)))
    except ValueError as e:
        raise InvalidUsage(str(e), status_code=400)


if __name__ == '__main__':
    app.run(debug=True)
